#!/usr/bin/python3
"""Programa cliente UDP que abre un socket a un servidor."""
import socket
import sys

try:
    IP_SRVR = sys.argv[1]
    # se pasa la IP del servidor
    PUERTO = int(sys.argv[2])
    # se pasa el puerto del servidor
    USU_SIP = sys.argv[4]
    METDO = sys.argv[3]
    EXPRS = sys.argv[5]
except IndexError:
    sys.exit('usage: client.py ip puerto register sip_address expires_value')

if METDO == 'register':
    # por si en prácticas posteriores no solo pone register
    pet = 'REGISTER SIP:' + USU_SIP + ' SIP/2.0\r\n\r\n'
    pet += 'expires in: ' + str(EXPRS) + '\r\n\r\n'

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

    my_socket.connect((IP_SRVR, PUERTO))
    print("Enviando:", pet)
    my_socket.send(bytes(pet, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))


print("Socket terminado.")

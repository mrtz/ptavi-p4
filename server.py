#!/usr/bin/python3
"""Clase (y programa principal) para un servidor de eco en UDP simple."""
import json
import socketserver
import sys
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """CLASE."""

    def register2json(self):
        """file json con usuarios registrados."""
        with open('registered.json', 'w') as f_json:
            json.dump(self.dicci, f_json)

    def json2registered(self):
        """comprueba el file json."""
        try:
            with open("registered.json", "r") as fijson:
                self.dicci = json.load(fijson)
        except FileNotFoundError:
            pass

    dicci = {}

    def handle(self):
        """handle method of the server class."""
        line = self.rfile.read()
        p = line.decode('utf-8').split(" ")

        DIR_IP = self.client_address[0]
        PORT = self.client_address[1]
        if p[0] == 'REGISTER':

            user_SIP = p[1].split(':')[1]
            address = str(DIR_IP)
            self.dicci[user_SIP] = DIR_IP
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            self.caduca = int(p[4])

            PEP8error = '%Y-%m-%d %H:%M:%S'
            Time = time.time() + self.caduca
            time_to_expire = time.strftime(PEP8error, time.gmtime(Time))
            tiempo_ahora = time.strftime(PEP8error, time.gmtime(time.time()))

            self.dicci[user_SIP] = {'IP': DIR_IP, 'expire': time_to_expire}
            if self.caduca == 0 or time_to_expire <= tiempo_ahora:
                del self.dicci[user_SIP]
                print(self.dicci)
            else:
                print(self.dicci)

        self.register2json()
        self.json2registered()


if __name__ == "__main__":
    LISTENP = int(sys.argv[1])
    serv = socketserver.UDPServer(('', LISTENP), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
